package com.example.roomwordsample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.example.roomwordsample.appdatabase.Entity.Word;
import com.example.roomwordsample.databinding.ActivityNewWordBinding;
import com.example.roomwordsample.viewmodel.WordViewModel;

public class NewWordActivity extends AppCompatActivity {
    private ActivityNewWordBinding binding;
    private WordViewModel wordViewModel;
    public static final String EXTRA_REPLY = "Word";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityNewWordBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        /*conecta el viewmodel con el adapter de la activity*/
        wordViewModel = new ViewModelProvider(this).get(WordViewModel.class);

        binding.buttonSave.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            if (TextUtils.isEmpty(binding.editWord.getText())) {
                setResult(RESULT_CANCELED, replyIntent);
            } else {
                String word = binding.editWord.getText().toString();
                insertNewWord(word);
                replyIntent.putExtra(EXTRA_REPLY, word);
                setResult(RESULT_OK, replyIntent);
            }
            finish();
        });
    }

    public void insertNewWord(@NonNull String data){
        Word word = new Word(data);
        wordViewModel.insertWord(word);
    }
}