package com.example.roomwordsample.appdatabase.Entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(
        tableName = "word_table",
        indices = {@Index(value = {"word"},unique = true )}
)
public class Word {

    /*Attributes*/
    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;

    /*Constructor*/
    public Word(@NonNull String word) { this.mWord = word; }

    /*getters*/
    public int getId(){ return this.id; }
    public String getWord(){ return this.mWord; }

    /*setters*/
    public void setId(@NonNull int id){ this.id = id; }
    public void setWord(@NonNull String word) { this.mWord = word; }
}
