package com.example.roomwordsample;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.roomwordsample.databinding.ActivityMainBinding;
import com.example.roomwordsample.view.ItemFragment;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        /*
        * Add a fragment programmatically
        * https://developer.android.com/guide/fragments/create
        * */
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
            .setReorderingAllowed(true)
            .add(R.id.fragment_container,ItemFragment.class,null)
            .commit();
        }

        /*define la acción del botón*/
        binding.fab.setOnClickListener(view ->{
            Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
            someActivityResultLauncher.launch(intent);
        });
    }

    // Callback button
    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK ) {
                    Toast.makeText(
                            getApplicationContext(),
                            result.getData().getStringExtra(NewWordActivity.EXTRA_REPLY),
                            Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(
                            getApplicationContext(),
                            R.string.empty_not_saved,
                            Toast.LENGTH_LONG).show();
                }
            });
}
