package com.example.roomwordsample.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.roomwordsample.databinding.HolderItemListBinding;

class WordViewHolder extends RecyclerView.ViewHolder {
    private final TextView itemView;

    private WordViewHolder(HolderItemListBinding itemView) {
        super(itemView.getRoot());
        this.itemView = itemView.content;
    }

    public void bind(String text) {
        itemView.setText(text);
    }

    static WordViewHolder create(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        HolderItemListBinding binding = HolderItemListBinding.inflate(inflater, parent, false);
        return new WordViewHolder(binding);
    }
}