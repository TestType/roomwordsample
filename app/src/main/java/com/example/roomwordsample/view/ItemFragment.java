package com.example.roomwordsample.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.roomwordsample.databinding.FragmentListBinding;
import com.example.roomwordsample.viewmodel.WordViewModel;

/**
 * A fragment representing a list of Items.
 */
public class ItemFragment extends Fragment {

    private FragmentListBinding binding;
    private WordViewModel wordViewModel;
    private WordListAdapter adapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemFragment() {
        adapter = new WordListAdapter(new WordListAdapter.WordDiff());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set the adapter
        binding.recyclerViewList .setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerViewList.setAdapter(adapter);

        /*conecta el viewmodel con el adapter de la activity*/
        wordViewModel = new ViewModelProvider(this).get(WordViewModel.class);
        wordViewModel.getAllWords().observe(getViewLifecycleOwner(), words -> {
            adapter.submitList(words);
        });
    }
}